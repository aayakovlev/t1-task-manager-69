package ru.t1.aayakovlev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.listener.EntityListener;
import ru.t1.aayakovlev.tm.service.ReceiverService;

@Component
public final class LoggerBootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener listener;

    @SneakyThrows
    public void init() {
        receiverService.receive(listener);
    }
}
