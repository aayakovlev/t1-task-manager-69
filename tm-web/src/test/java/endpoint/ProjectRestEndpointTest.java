package endpoint;

import marker.IntegrationCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.client.ProjectRestEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;

import java.util.List;

import static constant.ProjectTestConstant.*;

@Category(IntegrationCategory.class)
public final class ProjectRestEndpointTest {

    @NotNull
    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    @Before
    public void init() {
        client.save(PROJECT_ONE);
        client.save(PROJECT_TWO);
        client.save(PROJECT_THREE);
    }

    @After
    public void finish() {
        client.deleteAll();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_Count_Expect_Value() {
        client.save(PROJECT_FOUR);
        long count = client.count();
        Assert.assertEquals(4, count);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_DeleteAll_Expect_NoEntities() {
        client.deleteAll();
        long count = client.count();
        Assert.assertEquals(0, count);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_DeleteById_Expect_NullEntity() {
        client.deleteById(PROJECT_ONE.getId());
        Assert.assertThrows(ProjectNotFoundException.class,
                () -> client.findById(PROJECT_ONE.getId())
        );
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_ExistsById_Expect_True() {
        final boolean result = client.existsById(PROJECT_TWO.getId());
        Assert.assertTrue(result);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_FindAll_Expect_Entities() {
        @NotNull final List<ProjectDTO> results = client.findAll();
        Assert.assertNotEquals(0, results.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_FindById_Expect_ExistedEntity() {
        @NotNull final ProjectDTO result = client.findById(PROJECT_TWO.getId());
        Assert.assertEquals(PROJECT_TWO.getName(), result.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_Save_Expect_NewEntity() {
        client.save(PROJECT_FOUR);
        @NotNull final ProjectDTO newProject = client.findById(PROJECT_FOUR.getId());
        Assert.assertEquals(PROJECT_FOUR.getName(), newProject.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void When_Update_Expect_UpdatedEntity() {
        @NotNull final ProjectDTO existedProject = client.findById(PROJECT_THREE.getId());
        existedProject.setName("new project name");
        client.update(existedProject);
        @NotNull final ProjectDTO updatedProject = client.findById(PROJECT_THREE.getId());
        Assert.assertEquals("new project name", updatedProject.getName());
    }

}
