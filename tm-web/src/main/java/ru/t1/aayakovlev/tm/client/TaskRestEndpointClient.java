package ru.t1.aayakovlev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;

public interface TaskRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/tasks/";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/count")
    long count();

    @DeleteMapping("/delete")
    void deleteAll();

    @DeleteMapping("/delete/{id}")
    void deleteById(@PathVariable("id") @NotNull final String id);

    @GetMapping("/exists/{id}")
    boolean existsById(@PathVariable("id") @NotNull final String id);

    @NotNull
    @GetMapping("/findAll")
    List<TaskDTO> findAll();

    @NotNull
    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") @NotNull final String id);

    @NotNull
    @PutMapping("/save")
    TaskDTO save(@RequestBody @NotNull final TaskDTO task);

    @NotNull
    @PostMapping("/save/{id}")
    TaskDTO update(@RequestBody @NotNull final TaskDTO task);

}
