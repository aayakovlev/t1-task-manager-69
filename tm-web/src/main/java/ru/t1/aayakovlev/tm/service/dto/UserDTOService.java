package ru.t1.aayakovlev.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.api.service.dto.IUserDTOService;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserDTOService implements IUserDTOService {

    @Getter
    @NotNull
    @Autowired
    private UserDTORepository repository;

    @Override
    public long count() throws AbstractException {
        return getRepository().count();
    }

    @Override
    @Transactional
    public void deleteAll() {
        getRepository().deleteAll();
    }

    @Override
    @Transactional
    public void deleteById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        getRepository().deleteById(id);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsById(id);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() throws AbstractException {
        return getRepository().findAll();
    }

    @NotNull
    @Override
    public UserDTO findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<UserDTO> resultEntity = getRepository().findById(id);
        if (!resultEntity.isPresent()) throw new UserNotFoundException();
        return resultEntity.get();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        return getRepository().findByLogin(login);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO save(@Nullable final UserDTO user) throws EntityEmptyException {
        if (user == null) throw new EntityEmptyException();
        return getRepository().save(user);
    }

}
