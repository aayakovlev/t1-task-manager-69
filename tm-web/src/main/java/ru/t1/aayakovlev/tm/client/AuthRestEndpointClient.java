package ru.t1.aayakovlev.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;


public interface AuthRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/auth/";

    static AuthRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthRestEndpointClient.class, BASE_URL);
    }

    @PostMapping("/login")
    boolean login(@NotNull final String username, @NotNull final String password);

    @NotNull
    @GetMapping("/profile")
    UserDTO profile();

    @GetMapping("/logout")
    boolean logout();

}
