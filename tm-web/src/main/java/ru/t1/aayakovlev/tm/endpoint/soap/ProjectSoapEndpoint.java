package ru.t1.aayakovlev.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.aayakovlev.tm.api.endpoint.soap.IProjectSoapEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.util.UserUtil;

@Endpoint
public final class ProjectSoapEndpoint implements IProjectSoapEndpoint {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://aayakovlev.t1.ru/tm/dto/soap";

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    public ProjectCountResponse count(
            @RequestPayload @NotNull final ProjectCountRequest request
    ) throws AbstractException {
        return new ProjectCountResponse(service.countByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(
            @RequestPayload @NotNull final ProjectDeleteAllRequest request
    ) throws AbstractException {
        projectTaskService.deleteAllByUserId(UserUtil.getUserId());
        return new ProjectDeleteAllResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final ProjectDeleteByIdRequest request
    ) throws AbstractException {
        projectTaskService.deleteByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    public ProjectExistsByIdResponse existsById(
            @RequestPayload @NotNull final ProjectExistsByIdRequest request
    ) throws AbstractException {
        return new ProjectExistsByIdResponse(service.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(
            @RequestPayload @NotNull final ProjectFindAllRequest request
    ) throws AbstractException {
        return new ProjectFindAllResponse(service.findAllByUserId(UserUtil.getUserId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(
            @RequestPayload @NotNull final ProjectFindByIdRequest request
    ) throws AbstractException {
        return new ProjectFindByIdResponse(service.findByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(
            @RequestPayload @NotNull final ProjectSaveRequest request
    ) throws EntityEmptyException {
        @NotNull final ProjectDTO project = request.getProject();
        project.setUserId(UserUtil.getUserId());
        return new ProjectSaveResponse(service.save(project));
    }

    @NotNull
    @Override
    @ResponsePayload
    @PayloadRoot(localPart = "projectUpdateRequest", namespace = NAMESPACE)
    public ProjectUpdateResponse update(
            @RequestPayload @NotNull final ProjectUpdateRequest request
    ) throws EntityEmptyException {
        @NotNull final ProjectDTO project = request.getProject();
        project.setUserId(UserUtil.getUserId());
        return new ProjectUpdateResponse(service.save(project));
    }

}
