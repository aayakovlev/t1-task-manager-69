package ru.t1.aayakovlev.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_user", schema = "public", catalog = "task_manager")
public final class User {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "login", columnDefinition = "VARCHAR(64)")
    private String login;

    @NotNull
    @Column(name = "password", columnDefinition = "VARCHAR(255)")
    private String passwordHash;

    @NotNull
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

}
