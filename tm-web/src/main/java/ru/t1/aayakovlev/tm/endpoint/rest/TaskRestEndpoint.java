package ru.t1.aayakovlev.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.t1.aayakovlev.tm.api.endpoint.rest.ITaskRestEndpoint;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskDTOService service;

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/count", produces = APPLICATION_JSON_VALUE)
    public long count() throws AbstractException {
        return service.countByUserId(UserUtil.getUserId());
    }

    @Override
    @Secured({"ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete", produces = APPLICATION_JSON_VALUE)
    public void deleteAll() throws UserIdEmptyException {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @DeleteMapping(value = "/delete/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        service.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/exists/{id}", produces = APPLICATION_JSON_VALUE)
    public boolean existsById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findAll", produces = APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAll() throws AbstractException {
        return service.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findAllByProjectId/{projectId}", produces = APPLICATION_JSON_VALUE)
    public List<TaskDTO> findAllByProjectId(
            @PathVariable("projectId") @NotNull final String projectId
    ) throws AbstractException {
        return service.findAllByUserIdAndProjectId(UserUtil.getUserId(), projectId);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @GetMapping(value = "/findById/{id}", produces = APPLICATION_JSON_VALUE)
    public TaskDTO findById(
            @PathVariable("id") @NotNull final String id
    ) throws AbstractException {
        return service.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @PutMapping(value = "/save", produces = APPLICATION_JSON_VALUE)
    public TaskDTO save(
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException {
        task.setUserId(UserUtil.getUserId());
        return service.save(task);
    }

    @NotNull
    @Override
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    @PostMapping(value = "/save/{id}", produces = APPLICATION_JSON_VALUE)
    public TaskDTO update(
            @RequestBody @NotNull final TaskDTO task
    ) throws EntityEmptyException {
        task.setUserId(UserUtil.getUserId());
        return service.save(task);
    }

}
