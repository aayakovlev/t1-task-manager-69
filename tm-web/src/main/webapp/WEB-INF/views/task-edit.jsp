<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/header.jsp"/>
<h3>TASK EDIT</h3>
<form:form action="/tasks/edit/${task.id}" method="POST" modelAttribute="task">
    <form:hidden path="id"/>
    <div>
        <p class="no-btm-margin">Name</p>
        <form:input path="name" type="text"/>
    </div>
    <div>
        <p class="no-btm-margin">Description</p>
        <form:input path="description" type="text"/>
    </div>
    <div>
        <p class="no-btm-margin">Project</p>
        <form:select path="projectId">
            <form:option value="${null}" label=""/>
            <form:options items="${projects}" itemLabel="name" itemValue="id"/>
        </form:select>
    </div>
    <div>
        <p class="no-btm-margin">Status</p>
        <form:select path="status">
            <form:option value="${null}" label=""/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    <div>
        <p class="no-btm-margin">Created</p>
        <form:input path="created" type="date"/>
    </div>
    <form:button type="submit">Save Task</form:button>
</form:form>
<jsp:include page="../include/footer.jsp"/>
