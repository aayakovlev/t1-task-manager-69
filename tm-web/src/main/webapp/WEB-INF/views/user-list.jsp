<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp"/>

<h3>USER LIST</h3>

<table>
    <tr style="background-color: white;">
        <th style="width: 25%;">Id</th>
        <th style="width: 20%;">Login</th>
        <th style="width: 20%;">Roles</th>
        <th style="width: 10%;">Delete</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                <c:out value="${user.id}"/>
            </td>
            <td>
                <c:out value="${user.login}"/>
            </td>
            <td>
                <c:forEach var="role" items="${user.roles}">
                    <c:out value="${role}; "/>
                </c:forEach>
            </td>
            <td>
                <a href="/users/delete/${user.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<jsp:include page="../include/footer.jsp"/>
