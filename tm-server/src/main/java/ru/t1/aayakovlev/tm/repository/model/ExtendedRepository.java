package ru.t1.aayakovlev.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
public interface ExtendedRepository<E extends AbstractUserOwnedModel> extends BaseRepository<E> {

}
