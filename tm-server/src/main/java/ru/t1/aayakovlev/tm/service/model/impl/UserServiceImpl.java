package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.UserEmailExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserLoginExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.exception.field.RoleEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.model.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

@Service
public class UserServiceImpl extends AbstractBaseService<User, UserRepository> implements UserService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private UserRepository repository;

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Getter
    @NotNull
    @Autowired
    private SessionRepository sessionRepository;


    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (isEmailExists(login)) throw new UserEmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser;
        resultUser = getRepository().findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultUser;
        resultUser = getRepository().findByEmail(email);
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return getRepository().existsByLogin(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return getRepository().existsByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    @Transactional
    public void remove(@Nullable final User model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new UserNotFoundException();
        getTaskRepository().deleteByUserId(model.getId());
        getProjectRepository().deleteByUserId(model.getId());
        getSessionRepository().deleteByUserId(model.getId());
        getRepository().deleteById(model.getId());
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    @Transactional
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    @Transactional
    public @NotNull User update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
