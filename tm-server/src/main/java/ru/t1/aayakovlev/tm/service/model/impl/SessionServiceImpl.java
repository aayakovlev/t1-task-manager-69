package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.SessionNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.model.SessionRepository;
import ru.t1.aayakovlev.tm.service.model.SessionService;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class SessionServiceImpl extends AbstractExtendedService<Session, SessionRepository>
        implements SessionService {

    @Getter
    @NotNull
    @Autowired
    private SessionRepository repository;

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        getRepository().deleteByUserId(userId);
    }

    @Override
    public long count(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().countByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().existsByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        return getRepository().findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId, @Nullable final Comparator<Session> comparator)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (comparator == null) return findAll(userId);
        @NotNull Sort sort = getSort(comparator);
        return getRepository().findAllByUserId(userId, sort);
    }

    @NotNull
    @Override
    public Session findById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull Optional<Session> resultEntity = getRepository().findByUserIdAndId(userId, id);
        if (!resultEntity.isPresent()) throw new SessionNotFoundException();
        return resultEntity.get();
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Session model) throws AbstractException {
        if (model == null) throw new SessionNotFoundException();
        removeById(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (!existsById(userId, id)) throw new SessionNotFoundException();
        getRepository().deleteByUserIdAndId(userId, id);
    }

}
