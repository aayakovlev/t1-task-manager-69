package ru.t1.aayakovlev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface UserDTOService extends BaseDTOService<UserDTO> {

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws AbstractException;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws AbstractException;

    boolean isLoginExists(@Nullable final String login) throws AbstractException;

    boolean isEmailExists(@Nullable final String email) throws AbstractException;

    @NotNull
    UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException;

    void removeByLogin(@Nullable final String login) throws AbstractException;

    void removeByEmail(@Nullable final String email) throws AbstractException;

    @NotNull
    UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException;

    @NotNull
    UserDTO update(@Nullable final UserDTO user) throws AbstractException;

    @NotNull
    UserDTO update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException;

}
