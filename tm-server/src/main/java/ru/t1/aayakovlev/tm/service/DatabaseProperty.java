package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface DatabaseProperty {

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabaseURL();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHBM2DLL();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseFormatSql();

    @NotNull
    String getDatabaseInitToken();

    @NotNull
    String getDatabaseSchema();

    @NotNull
    String getUseSecondCache();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getUseRegionPrefix();

    @NotNull
    String getHZConfFile();

    @NotNull
    String getFactoryClass();

}
