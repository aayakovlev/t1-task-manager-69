package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class UserShowProfileListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Show user profile.";

    @NotNull
    public static final String NAME = "user-show-profile";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userShowProfileListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW USER PROFILE]");

        @NotNull final UserShowProfileRequest request = new UserShowProfileRequest(getToken());
        @Nullable final UserShowProfileResponse response = authEndpoint.profile(request);
        @Nullable final UserDTO user = response.getUser();
        if (user == null) return;

        showUser(user);
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}
